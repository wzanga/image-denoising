from commons.datagen import Generator
from commons.display import show2
from training.trainer_dncnn import Trainer_DnCNN
from training.trainer_rednet import Trainer_REDnet
from training.trainer_memnet import Trainer_MemNet
import numpy as np


if __name__=="__main__":
    depth = 6
    channels = 1
    train_data = './data/Set12'
    save_dir = './models'
    batch_size=128
    sigma = 5
    val_ratio = 0.01
    resume = True
    nepoch = 10
    lr = 1e-2
    save_freq = 1
    optimizer = 'Adam'

    #Generate the training data
    #ds = Generator(datadir=train_data, ext='png', patch_size=50, batch_size=128, nrotflip=1, channels=1)
    #ds.getbatches()
    #data = ds.getset(sigma=sigma)
    #for t in range(5):
    #    i = np.random.randint(len(data[0]))
    #    show2(data[0][i][:,:,0], data[1][i][:,:,0])


    #trainer = Trainer_DnCNN(depth, channels, train_data, save_dir, batch_size, sigma, val_ratio, resume, nepoch, lr, save_freq, optimizer)
    #trainer.distributed_training()

    #trainer = Trainer_REDnet(depth, channels, train_data, save_dir, batch_size, sigma, val_ratio, resume, nepoch, lr, save_freq, optimizer)
    #trainer.distributed_training()

    #trainer = Trainer_MemNet([1,depth], channels, train_data, save_dir, batch_size, sigma, val_ratio, resume, nepoch, lr, save_freq, optimizer)
    #trainer.distributed_training()
