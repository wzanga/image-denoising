from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
print("TensorFlow version: {}".format(tf.__version__) )

from tensorflow.keras.callbacks import CSVLogger, ModelCheckpoint, LearningRateScheduler
from commons.datagen import Generator
import datetime
import glob
import sys
import os

class Trainer:

    def __init__(self, arch, depth, channels, train_data, save_dir, batch_size, sigma, val_ratio, resume, nepoch, lr, save_freq, optimizer):
        """ Constructor
            arch        : string  : identification of the architecture
            depth       : integer : depth of the architecture
            channels    : integer : number of channels
            train_data  : string  : file containing the training data
            save_dir    : string  : directory containing the models
            batch_size  : integer : batch size
            sigma       : float   : noise level
            val_ratio   : float   : ratio of the validation data
            resume      : boolean : resume from the last saved mode ?
            nepoch      : integer : number of epochs
            lr          : float   : initial learning rate
            save_freq   : integer : save the model every {save_freq} epochs
            optimizer   : string  : name of the optimizer
        """
        self.arch = arch
        self.depth = depth
        self.channels = channels
        self.train_data = train_data
        self.batch_size = batch_size
        self.sigma = sigma
        self.val_ratio = val_ratio
        self.resume = resume
        self.nepoch = nepoch
        self.lr = lr
        self.save_freq = save_freq
        self.optimizer = optimizer
        self.generator = Generator(datadir=self.train_data)
        self.save_dir = '{}/{}.{}.{}.{}'.format(save_dir,arch,depth,channels,sigma)
        if not os.path.exists(self.save_dir): os.mkdir(self.save_dir)
        return


    def lr_schedule(self,epoch):
        """ Compute the recommended value for the learning_rate depending on the epoch """

    def get_optimizer(self):
        """ Create the optimizer """

    def get_architecture(self):
        """ create an empty architecture """

    def custom_loss(self, y_true, y_pred):
        """ definition of the loss function used """
        import tensorflow.keras.backend as K
        L1 = K.mean(K.abs(y_true - y_pred), axis=-1)
        L2 = K.mean(K.square(y_true - y_pred), axis=-1)
        L3 = 100.-tf.reduce_mean(tf.image.psnr(y_true, y_pred, max_val=self.generator.pixel_max))
        L4 = 1.0-tf.reduce_mean(tf.image.ssim(y_true, y_pred, filter_size=3, max_val=self.generator.pixel_max))
        w = [1-.84 , .0, .0, .84]
        loss = w[0]*L1 +  w[1]*L2 + w[2]*L3 +  w[3]*L4
        return loss

    def log(self,*args,**kwargs):
        """ display the current state while training """
        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S:"),*args,**kwargs)

    def find_last_checkpoint(self,):
        """ Find the index of the last model saved """
        file_list = glob.glob(os.path.join(self.save_dir,'model_*.hdf5'))
        if file_list :
            file_list.sort(key=os.path.getmtime)
            f = file_list[-1]
            start = f.find("model_") + len("model_")
            end   = f.find(".hdf5")
            return int(f[start:end])
        else:
            return 0

    def get_model(self,):
        """ get the model last model saved """
        initial_epoch  = self.find_last_checkpoint()
        if self.resume==False or initial_epoch==0:
            return None, 0
        print("Loading the pretained model <"+model_path+">...")
        model_path = os.path.join(self.save_dir , 'model_' + '{0:03}'.format(initial_epoch) +'.hdf5')
        model = tf.keras.models.load_model(filepath = model_path, compile=False)
        return model, initial_epoch


    def distributed_training(self):
        """ training the model with a distributed strategy """
        #define th strategy
        strategy = tf.distribute.MirroredStrategy()
        print('\n\nNumber of devices:{}'.format(strategy.num_replicas_in_sync) + '\n')
        #load training data
        data = self.generator.getset(sigma=self.sigma)
        #LOGFILE
        with open(self.save_dir + "/strategy.txt","w") as f :
            BUFFER_SIZE = len( data[0] )
            BATCH_SIZE_PER_REPLICA = self.batch_size
            GLOBAL_BATCH_SIZE = BATCH_SIZE_PER_REPLICA * strategy.num_replicas_in_sync
            EPOCHS = self.nepoch
            f.write("Number of devices:{}".format(strategy.num_replicas_in_sync) + "\n")
            f.write("BATCH_SIZE_PER_REPLICA: {}".format(BATCH_SIZE_PER_REPLICA) + "\n")
            f.write("GLOBAL_BATCH_SIZE: {}".format(GLOBAL_BATCH_SIZE) + "\n")
            f.close()

        #callback to save the models as it's being optimzed
        print("\nAdding {checkpoints, csv_logger, lr_scheduler, tensorboard} callbacks...")
        checkpointer = ModelCheckpoint(os.path.join(self.save_dir,'model_{epoch:03d}.hdf5'), verbose=0, save_weights_only=False, save_freq = self.save_freq)
        csv_logger = CSVLogger(os.path.join(self.save_dir,'log.csv'), append=True, separator=',')
        lr_scheduler = LearningRateScheduler(self.lr_schedule)
        logdir = self.save_dir + "/logs/" + self.arch + datetime.datetime.now().now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)
        callbacks = [checkpointer, csv_logger, lr_scheduler]

        with strategy.scope():
            print("\nInitializing an empty model based upon the " + self.arch  + str(self.depth)+ " architecture...")
            model = self.get_architecture()

            pretrained_model, initial_epoch = self.get_model()
            if pretrained_model is not None :
                print("Transferring the weights from the pretrained model to the empty model...")
                model.set_weights(pretrained_model.get_weights())

            print("Compiling the model...")
            model.compile(optimizer=self.get_optimizer(), loss=self.custom_loss , metrics=["mse","mae"])

        print('\n\n')
        model.fit(x=data[0], y=data[1], shuffle=True, epochs = EPOCHS, batch_size=GLOBAL_BATCH_SIZE, validation_split=self.val_ratio,  callbacks = callbacks, initial_epoch = initial_epoch)
        return
