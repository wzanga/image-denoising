from training.trainer import Trainer
from architectures.dncnn import DnCNN
import tensorflow as tf

class Trainer_DnCNN(Trainer):

    def __init__(self, depth, channels, train_data, save_dir, batch_size, sigma, val_ratio, resume, nepoch, lr, save_freq, optimizer):
        """ Constructor
            arch        : string  : identification of the architecture
            depth       : integer : depth of the architecture
            channels    : integer : number of channels
            train_data  : string  : file containing the training data
            save_dir    : string  : directory containing the models
            batch_size  : integer : batch size
            sigma       : float   : noise level
            val_ratio   : float   : ratio of the validation data
            resume      : boolean : resume from the last saved mode ?
            nepoch      : integer : number of epochs
            lr          : float   : initial learning rate
            save_freq   : integer : save the model every {save_freq} epochs
            optimizer   : string  : name of the optimizer
        """
        super().__init__("DnCNN", depth, channels, train_data, save_dir, batch_size, sigma, val_ratio, resume, nepoch, lr, save_freq, optimizer)


    def lr_schedule(self,epoch):
        """ Compute the recommended value for the learning_rate depending on the epoch """
        factor = (1 - 5./100)**epoch
        factor = factor/2.
        lr = self.lr * factor
        self.log('current learning rate is %2.8f' %lr)
        return lr

    def get_optimizer(self):
        """ Create the optimizer """
        if self.optimizer == 'SGD':
            return tf.keras.optimizers.SGD(name='SGD', decay=0.96, learning_rate=self.lr, momentum=0.9, nesterov=True)
        else:
            return tf.keras.optimizers.Adam(name='Adam', learning_rate=self.lr)

    def get_architecture(self):
        """ create an empty architecture """
        model = DnCNN(d=self.depth,c=self.channels)
        return model
