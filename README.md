# [Deep Learning] Removing noise from images with convolutional neural networks 
<img align="center"  src=illustration/octo2.png width="900">

This project implements Deep Learning based image denoisers based upon recent development in the litterature as compare their
performances to a semi-deterministic algorithm. The neural network architectures considered in this study are all 
residual networks meant to solve for a noiseless image $`x`$  in the stochactic problem  

```math
y = H(x) + n
```

where $`H(.)`$ is a degradation function and $`n`$ is a noisy image abiding by an approximately known Poisson-Gauss distribution.
For the networks used in this project, the formal solution $`x = Argmax P[ y |x, n, H(.) ]`$ is approximated by a forward 
pass through a series of convolutions and ReLU activation functions.

Noise estimation             |  Execution time
:-------------------------:|:-------------------------:
![](illustration/noise.png)  |  ![](illustration/time.png)

**Bibliography** :
* *Beyond A Gaussian Denoiser, : residual Learning of Deep CNN for Image denoising, Kai Zhang, Wangmeng Zuo, Yunjin Chen, Deyu Meng, Lei Zhang, 13 aug 2016.*,
* *Image Restoration Using Very Deep Convolutional Encoder-Decoder Networks with Symmetric Skip Connections Xiao-Jiao Mao, Chunhua Shen, Yu-Bin Yang, Sept 2016*
* *MemNet : A Persistent Memory Network for Image Restoration, Ying Tai, Jian Yang, Xiaoming Liu, Chunyan Xu*
* *Non-Local Recurrent Network for Image Restoration, Ding Liu, Bihan Wen, Yuchen Fan, Chen Change Loy, Thomas S. Huang, 11 Dec 2018*
