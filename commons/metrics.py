import numpy as np

#%% Performance metrics
def MRE(x,y):
    """ compute the relative errors between x and y"""
    fxy = np.maximum(np.abs(x), np.abs(y))
    np.where(fxy==0, 1,fxy)
    error = np.abs((y-x) * 100./fxy)
    error = np.nan_to_num(error, nan=0, posinf=0)
    return[error.min(), error.mean(), error.max()]

def PSNR(x,y, pixel_max):
    """ peak singal to noise ratio """
    from skimage.measure import compare_psnr
    return compare_psnr(x,y, data_range=pixel_max)

def SSIM(x,y, pixel_max):
    """ structural similarity """
    from skimage.measure import compare_ssim
    return compare_ssim(x,y, data_range=pixel_max)

def summary(x,z,pixel_max, prec=None):
    """ Compute all the metrics"""
    [rmin, rmoy, rmax] = MRE(x,z)
    psnr = PSNR(x,z,pixel_max)
    ssim = SSIM(x,z,pixel_max)
    if prec is not None:
        rmin = round(rmin,prec)
        rmoy = round(rmoy,prec)
        rmax = np.round(rmax,prec)
        psnr = round(psnr,prec)
        ssim = round(ssim,prec+2)
    return [rmin,rmoy,rmax,psnr,ssim]
