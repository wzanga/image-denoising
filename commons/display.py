
import matplotlib.pyplot as plt


def show2(x,y):
    """ Display the deconvoled image (X), the renoised image (Y) and the NL-bayes denoised image (Z)
        Input :
            x : noisy patch : ndarray
            y : clean patch : ndarray
        Output: None
    """
    cmap = "gray"
    plt.figure()

    ax0 = plt.subplot(121)
    ax0.imshow(x,  cmap=cmap)
    ax0.set_title('Noisy patch')

    ax1 = plt.subplot(122, sharex=ax0, sharey=ax0)
    ax1.imshow(y,  cmap=cmap)
    ax1.set_title('Clean patch')

    plt.setp(ax0.get_xticklabels(), visible=False)
    plt.setp(ax0.get_yticklabels(), visible=False)
    ax0.tick_params(axis='both', which='both', length=0)
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax1.get_yticklabels(), visible=False)
    ax1.tick_params(axis='both', which='both', length=0)

    plt.tight_layout()
    plt.show()
    return
