# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Module name : datagen.py
# Description : this module generates the data needed for training the Deep CNN
#               and for the duration benchmark (NLBayes vs Deep CNN)
# Author : Williams ZANGA (Thales Services)
# Date : 2020
#-------------------------------------------------------------------------------
import PIL.Image as pil_image
import numpy as np
import tqdm
import glob
import os

class Generator(object):

    def __init__(self, datadir, ext='', patch_size=-1, batch_size=-1, nrotflip=-1, channels=-1):
        """ Class initializer
            Input :
                datadir : directory containing the data
                setname : Generator name : string
                ext : image extensions : string
                patch_size : patch size : integer
                batch_size : batch size : integer
                nrotflip   : number of rotation and flip considered for data augmenation
                channels   : number of image channels : integer
            Output: None
        """
        self.datadir = datadir
        self.patch_size = patch_size
        self.batch_size = batch_size
        self.nrotflip = nrotflip
        self.channels = channels
        self.pixel_max = 2**8-1
        self.image_files = sorted( glob.glob(os.path.join(datadir, "*.{}".format(ext))) )

    def rotflip(self, x, mode):
        """ Data augmenation function that rotate and/or flips an image
            Input :
                x : image : ndarray
                mode : index of the geoemtric transformation applied : integer
            Output:  new image
        """
        if mode == 0:
            return x
        elif mode == 1:
            return np.flipud(x)
        elif mode == 2:
            return np.rot90(x)
        elif mode == 3:
            return np.flipud(np.rot90(x))
        elif mode == 4:
            return np.rot90(x, k=2)
        elif mode == 5:
            return np.flipud(np.rot90(x, k=2))
        elif mode == 6:
            return np.rot90(x, k=3)
        elif mode == 7:
            return np.flipud(np.rot90(x, k=3))

    def getpatches(self, idx):
        """ Extract the patches from a given image
            Input :
                idx : index of the image in the self.image_files list : integer
            Output:
                patches : list of patches [patch_1, patch_2,......,patch_N]
                with patch_i being a numpy ndarray of shape (height x width x channels)
        """
        #read and convert the image
        if self.channels == 1:
            original = pil_image.open(self.image_files[idx]).convert('L')
        elif self.channels == 3:
            original = pil_image.open(self.image_files[idx]).convert('RGB')
        else: raise ValueError('Only Grayscale and RGB images are handled')
        #extract patches
        patches = []
        w,h = original.size
        for i in range(h//self.patch_size):
            for j in range(w//self.patch_size):
                #crop image
                top = i * self.patch_size
                bottom = top + self.patch_size
                left = j * self.patch_size
                right = left + self.patch_size
                label = original.crop((left, top, right, bottom))
                #keep only non monochromatic images
                if label.getbbox():
                    for k in range(self.nrotflip):
                        z = np.asarray(self.rotflip(label, mode=k), dtype=np.float32)
                        z = z.reshape(z.shape[0], z.shape[1], self.channels)
                        patches.append(z)
        return patches

    def getbatches(self, verbose=True, rme=True):
        """ Create the training data containing the patches from all the images in the Generator and save it into a file
            Input :
                verbose : indicate wether or not percentage of processing is displayed on the screen : boolean
                rme : indicate wether or not excessed data are removed so as to have a whole number of the batches : boolean
            Output:
        """
        batches=[]
        n = len(self.image_files)
        #extract all the patches
        for idx in tqdm.tqdm(iterable=range(n), desc="Creating patches from images", ncols=100):
            patches = self.getpatches(idx)
            for patch in patches:
                batches.append(patch)
        batches = np.asarray(batches,dtype=np.float64)
        #remove excess data :  len(data) = batch_size * q + r
        if(rme):
            n = len(batches)
            q = n//self.batch_size
            if q!=0:
                print('Removing excess data...')
                r = n%self.batch_size
                batches = np.delete(batches, range(r), axis=0)
            else:
                raise RuntimeError('Not enough data to create batches of size {}'.format(self.batch_size))
        #save data
        print("Saving the batches into <" + self.datadir + ">...")
        print('Shape of Generator = {}'.format(batches.shape))
        np.save( self.datadir , batches)
        return


    def load(self):
        """ Load the Generator from its file and save it into a file
            Input : None
            Output: Generator : numpy ndarray
        """
        if not os.path.exists(self.datadir):
            raise RuntimeError("<" + self.datadir + '> does not exist !')
        print("Loading <" + self.datadir + '>...')
        return np.load(self.datadir +'.npy')


    def getset(self, mu=0, sigma=0):
        """ Get the training set
            Input :
                mu : mean value of the additional noise : float
                sigma : noise level : float
            Output: batches : numpy ndarray
        """
        # readjust size
        batches = [None]*2
        batches[1] = self.load()
        batches[0] = batches[1] + np.random.normal(mu, sigma, batches[1].shape)
        batches = np.asarray(batches) / self.pixel_max
        #display shapes
        print('Shape of Generator = ' + str(batches.shape))
        print('<noisy> |min:', batches[0].min() ,' |max:',  batches[0].max() )
        print('<clean> |min:', batches[1].min() ,' |max:',  batches[1].max() )
        return batches
