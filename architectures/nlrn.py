#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 14:38:08 2019

@author: zangaw

"""
import tensorflow as tf
from tensorflow.keras.layers import Conv2D, Input, BatchNormalization, Activation, Add, Reshape, Permute
from tensorflow.keras import Model
import tensorflow.keras.backend as K


class BNReLU:
    """Apply : Batch Norm + ReLU """
    def __init__(self, name="", **kwargs):
        self.b0 = BatchNormalization(axis=3, momentum=0.9, epsilon=0.001, name=name+"BN")
        self.b1 = Activation('relu', name=name+ "relu")

    def __call__(self, inputs):
        x = self.b0(inputs)
        return self.b1(x)

class BNReLUConv:
    """Apply :Batch Norm + ReLU + conv2D"""
    def __init__(self, f=64, k=3, s=1, p='same', name="", **kwargs):
        self.b0 = BNReLU(name=name)
        self.b1 = Conv2D(filters=f, kernel_size=k, strides=s, padding=p, kernel_initializer='Orthogonal', use_bias = True, name=name+"conv2D")

    def __call__(self, inputs):
        x = self.b0(inputs)
        return self.b1(x)


class InBlock:
    """ Block that tranforms the input x^{0} into s^{0} """
    def __init__(self,f=64, k=3, s=1, p='same', name="f_input-", **kwargs):
        self.b0 = BatchNormalization(axis=3, momentum=0.9, epsilon=0.001, name=name+"BN")
        self.b1 = Conv2D(filters=f, kernel_size=k, strides=s, padding=p, use_bias=False, name=name + "conv2D")
        return

    def __call__(self, x):
        x = self.b0(x)
        return self.b1(x)


class OutBlock:
    """ Block that tranforms the last recurrent state s^{T}
        into the ouput sate y^{T}
    """
    def __init__(self,f=1, k=3, s=1, p='same', name="f_output-", **kwargs):
        self.b0 = BNReLUConv(f=f, k=k, s=s, p=p, name=name)

    def __call__(self, s):
        return self.b0(s[0])


class ResidualBlock:
    """ Implement the recurrent function """
    def __init__(self, f=64, k=3, s=1, name="", **kwargs):
        #bn + relu + non local module
        self.b0 = BNReLU(name=name)
        self.b1 = NonLocalModule(name=name + "NLmodule-")
        #batch norm + relu + conv
        self.b2 =  BNReLUConv(f=f, k=k, s=s, name= name + "A-" )
        self.b3 =  BNReLUConv(f=f, k=k, s=s, name= name + "B-" )
        #Adder
        self.b4 = Add(name=name + 'adder')

    def __call__(self,s,s0):
        """ Input : s^(t-1) and  s^(0)
            Output: s^(t) = (st_feat, st_corr)
        """
        #normalize s
        s = [ self.b0(s[0]) , s[1] ]
        #normalize s0
        s0 = [ self.b0(s0[0]), s0[1] ]
        #non local module
        z , st_corr = self.b1(s)
        #normalize z
        z = self.b2(z)
        z = self.b3(z)
        #identity skip connection
        st_feat = self.b4( [z , s0[0] ])
        print(st_corr)
        print(st_feat)
        return (st_feat, st_corr)


class NonLocalModule:
    """ Non-local module of the network """
    def __init__(self, l=32, m=64, k=1, s=1, q=45, p="same", name="", **kwargs):
        self.theta = Conv2D(filters=l, kernel_size=k, strides=s, padding=p, name=name + "THETA")
        self.phi   = Conv2D(filters=l, kernel_size=k, strides=s, padding=p, name=name + "PHI")
        self.g     = Conv2D(filters=m, kernel_size=k, strides=s, padding=p, name=name + "G")
        self.b4 = Add(name=name + 'adder')
        self.b5 = Activation('softmax', name=name+ "softmax")
        self.q = q
        self.m = m
        self.l = l
        return

    def __call__(self,s):
        """
        Input: s^(t-1)= s(t-1)_feat, s(t-1)_corr
        Output: (z, st_corr) where z if the non-local version of st_feat
        """
        # 3-filtering
        st = self.theta(s[0])
        sp = self.phi(s[0])
        sg = self.g(s[0])
        print('-------------CONVS----------------------')
        print(st)
        print(sp)
        print(sg)
        print('-----------------------------------')
        # reshaping
        st_reshaped = Reshape( (st.shape[1] * st.shape[2], st.shape[3]) )(st)
        sp_reshaped = Reshape( (sp.shape[3] , sp.shape[1] * sp.shape[2]))(sp)
        sg_reshaped = Reshape( (sg.shape[1] * sg.shape[2], sg.shape[3]) )(sg)
        print('-------------RESHAPE----------------------')
        print(st_reshaped)
        print(sp_reshaped)
        print(sg_reshaped)

        print('-------------ST_CORR----------------------')
        # s_theta * s_phi
        #u = K.dot(st_reshaped , sp_reshaped)
        st_corr = tf.matmul(st_reshaped , sp_reshaped)
        if s[1] is not None:
            st_corr = self.b4([st_corr , s[1] ])
        print(st_corr)

        print('---------------SOFTMAX-MULT----------------------------')
        u = self.b5(st_corr)
        #u = K.dot(u , sg_reshaped)
        u = tf.matmul(u, sg_reshaped)
        #u = Reshape( (u.shape[1] , u.shape[3]))(u)
        print(u)

        print('---------------RESHAPE S[0] ADD----------------------------')
        v =  Reshape( (s[0].shape[1] * s[0].shape[2], s[0].shape[3]) )(sg)
        print(v)
        u = self.b4([u,v])
        print(u)
        print('---------------RESHAOE----------------------------')
        u = Reshape( ( s[0].shape[1] , s[0].shape[1], s[0].shape[3]) )(u)
        print(u)
        print("------------------------------------")
        return [u,st_corr]


class NLRN:
    """ Non-Local Recurrent Network """
    def __new__(self, T, c=1, name="NLRN",**kwargs):
        self.c = c
        self.T = T
        self.f_input = InBlock()
        self.f_outpout = OutBlock(f=c)
        self.residual_blocks = [ ResidualBlock(name="RB-" + str(i) +'-') for i in range(T) ]
        return self.__call__(self)

    def __call__(self):
        x = Input( shape=(40,40, self.c) , name='Input')
        #preprocess
        s0 = [ self.f_input(x) , None]
        s = s0
        # residual block loop
        for residaul_block in self.residual_blocks:
            s = residaul_block(s,s0)
        #postprocess
        y = self.f_outpout(s)
        return Model(inputs=x, outputs=y, name="NLRN")


if __name__=="__main__":
    model = NLRN(T=2, c=1, name="RB-")
    model.summary()
