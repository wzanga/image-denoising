
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 14:18:34 2019

@author: zangaw
"""
from tensorflow.keras.layers import Conv2D, Input, LeakyReLU, MaxPooling2D, concatenate, UpSampling2D
import numpy as np
from tensorflow.keras import Model



class ConvLR:
    """  Sequentially apply : conv2D and Leaky-ReLU to the inputs """
    def __init__(self, f=48, k=(3,3), s=(1,1), a=0.1, name="", **kwargs):
        self.b1 = Conv2D(filters= f, kernel_size=k, strides=s, padding='same', kernel_initializer='Orthogonal', name=name+"conv2D")
        self.b2 = LeakyReLU(alpha=a, name=name + 'leaky-relu')

    def __call__(self, inputs):
        x = self.b1(inputs)
        return self.b2(x)

class ConvLRP:
    """  Sequentially apply : conv2D and Leaky-ReLU and MaxPool2D to the inputs """
    def __init__(self, f=48, k=(3,3), s=(1,1), a=0.1, p=(2,2), name="", **kwargs):
        self.b1 = ConvLR(f,k,s,a,name,**kwargs)
        self.b2 = MaxPooling2D(pool_size=p, name= name +'maxpool2D')

    def __call__(self, inputs):
        x = self.b1(inputs)
        return self.b2(x)


class UC2ConvLR:
    """  Sequentially apply : [ Upscale2D | Concact | Conv + Leaky ReLu | Conv + Leaky ReLu ] to the inputs """
    def __init__(self, f=[96,96], k=(3,3), s=(1,1), a=0.1, p=(2,2), u=(2,2), name="", **kwargs):
        self.b1 = UpSampling2D(size=u, name=name + 'upsample2dD')
        self.b2 = ConvLR(f[0],k,s,a,name=name + "CLR-A-",**kwargs)
        self.b3 = ConvLR(f[1],k,s,a,name=name + "CLR-B-",**kwargs)

    def __call__(self,x,xskip):
        x = self.b1(x)
        x = concatenate([x,xskip])
        x = self.b2(x)
        return self.b3(x)


class ConvBlock:
    """ Compute the activation of the block
            x --->[ Conv]---> x
    """
    def __init__(self, f=1, k=(3,3), s=(1,1),name=""):
        self.b1 = Conv2D(filters= f, kernel_size=k, strides=s, kernel_initializer='Orthogonal', padding='same', use_bias=True, name=name + "conv2D")

    def __call__(self,x):
       return self.b1(x)


class Noise2Noise:
    """ Noise2Noise class
        depth : desired depth such that (depth-3 mirror blocks | 3 conv blocks)
        channels: 1 for grayscale and 3 for colored
    """
    def __new__(self, d=13, c=1, mps=2, imsize=40, imsizemin=7):
        #architecture informations
        self.c=c
        #compute the "effective" depth depending of the maxpool
        self.d = 2 * int( np.log(imsize/imsizemin) / np.log(mps)) + 3
        if (d-3)%2==0:
            self.d = min(self.d, d)
        return self.__call__(self)

    def __call__(self):
        """ Forward propagation """
        residual = Input( shape=(None,None, self.c) , name='Input')
        X=[residual]
        x = residual
        s = (self.d-3)//2
        # conv + LR
        x = ConvLR(name="ENC0-")(x)
        # (depth-3)/2 CONV+LR+POOL
        for i in range(s):
            x = ConvLRP( name="ENC"+str(i+1)+"-")(x)
            X.append(x)
        X.pop()
        # conv + LR
        x = ConvLR(name="ENC"+str(s+1) +"-")(x)
        # (depth-3)/2 UPSAMPLE+CONCAT+CONV+LR+CONV+LR
        for i in range(s,1,-1):
            x = UC2ConvLR(name="DEC"+str(i)+"-")(x, X.pop() )
        # last-1 block
        x = UC2ConvLR(f=[64,32],name="DEC1-")(x, X.pop() )
        # conv
        x = ConvBlock(f=self.c,name="DEC0-")(x)
        return Model(inputs=residual, outputs=x, name="Noise2Noise")

if __name__=="__main__":
    model = Noise2Noise(d=13)
    model.summary()
