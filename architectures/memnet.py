#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 15:23:15 2019

@author: zangaw
"""
from tensorflow.keras.layers import Conv2D, Input, BatchNormalization, Activation, Concatenate, Add
from tensorflow.keras import Model

class BNReLUConv:
    """ BNReLuConv Block
        x --->[ BN | ReLu | Conv ]---> x
    """
    def __init__(self,f=64, k=3, s=1, p='same', name=''):
        self.b1 = BatchNormalization(axis=-1, name =name + '-bn')
        self.b2 = Activation('relu', name= name + '-relu')
        self.b3 = Conv2D(filters=f, kernel_size=k, strides=s, padding=p, use_bias=False, kernel_initializer='glorot_normal', name=name+'-conv')

    def __call__(self, x):
        x = self.b1(x)
        x = self.b2(x)
        return self.b3(x)

class GateUnit:
    """ GateUnit Block
        x --->[ BN | ReLU | Conv]---> x
    """
    def __init__(self,f=64, k=1, s=1, p='same', name=''):
         self.b1 = BNReLUConv(f=f, k=k, s=s, p=p, name=name + '-gateunit')

    def __call__(self,x):
        return self.b1(x)

class ResidualBlock:
    """ Residual Block (cf: https://arxiv.org/abs/1512.03385)
        x --->[ BNReLUConv]--[ BNReluConv ]--[residual + x ]--> x
    """
    def __init__(self, f=64, k=3, s=1, name=''):
        self.b1 = BNReLUConv(f=f, k=k, s=s, name=name + '-brcA')
        self.b2 = BNReLUConv(f=f, k=k, s=s, name=name + '-brcB')
        self.b3 = Add(name=name+'-output')

    def __call__(self,x):
        residual = x
        x = self.b1(x)
        x = self.b2(x)
        return self.b3([x,residual])

class  MemoryBlock:
    """ Memory Block
        x --->[ ResidualBlock]-/-[ ResidualBlock]-/-[ ResidualBlock]-/- .....[ ResidualBlock]-/--[GateUnit]-> x
    """
    def __init__(self, f, n_resblock, name=''):
        self.gate_unit = GateUnit(f=f, name=name)
        self.residual_blocks =[ ResidualBlock(f=f, name=name + 'R' + str(i+1)) for i in range(n_resblock) ]

    def __call__(self,x,ys):
        """
            ys : long-term memory from previous memory blocks
            x : input block
        """
        xs = []
        for resblock in self.residual_blocks:
            x = resblock(x)
            xs.append(x)
        c = Concatenate(axis=3)(ys + xs)
        out = self.gate_unit(c)
        ys.append(out)
        return out

class MemNet:
    """ Memory Network for image denoising
    """
    def __new__(self, f=64, c=1, n_memblock=1, n_resblock=1, name="MemNet", mode="basic"):
        #feature extractor
        self.feature_extractor =  BNReLUConv(f=f, name= 'FENet')
        #reconstructor
        self.reconstructor = BNReLUConv(f=c, name= 'ReconNet')
        #memory blocks
        self.dense_memory = [ MemoryBlock(f=f, n_resblock=n_resblock, name='M' + str(i+1)) for i in range(n_memblock)]
        #additional params
        self.name = name
        self.mode = mode
        self.c = c
        return self.__call__(self, mode)

    def __call__(self, mode):
        x = Input( shape=(None,None, self.c) , name='Input')
        if mode=="basic":
            y = self.__basic__(self, x)
        else:
            y = self.__multi_supervised__(self, x)
        return Model(inputs=x, outputs=y, name=self.name)

    def __basic__(self,x):
        residual = x
        x = self.feature_extractor(x)
        ys=[x]
        for memory in self.dense_memory :
            x = memory(x,ys)
        x = self.reconstructor(x)
        return Add(name='Output')([x,residual])

    def __multi_supervised__(self,x):
        residual = x
        x = self.feature_extractor(x)
        mid_feat = [] # list of output of each memory block
        ys = [x]    #
        #memory blocks passage
        for memory_block in self.dense_memory:
            x = memory_block(x,ys)
            mid_feat.append(x)
        #reconstuct
        pred = [ Add()([z,residual]) for z in mid_feat ]
        if len(pred)>1:
            pred = [ Concatenate(axis=3)(pred) ]
        return self.reconstructor(pred[0])

if __name__=="__main__":
    model = MemNet(n_memblock=3, n_resblock=2, mode="multi")
    model.summary()
