#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 16:02:43 2019

@author: zangaw

This code implements a denoising convolutional neural network (DnCNN)
with tensorflow 2.0
"""
from tensorflow.keras.layers import Input, Conv2D, BatchNormalization, Activation, Subtract
from tensorflow.keras import Model


class ConvReLU:
    """  Sequentially apply : conv2D and ReLU to the inputs """
    def __init__(self, f=64, k=3, s=1, name="", **kwargs):
        self.b1 = Conv2D(filters= f, kernel_size=k, strides=s, padding='same', name=name+"conv2D")
        self.b2 = Activation('relu', name=name+ "relu")

    def __call__(self, inputs):
        x = self.b1(inputs)
        return self.b2(x)

class ConvBNReLU:
    """Apply : conv2D + Batch Norm + ReLU """
    def __init__(self, f=64, k=3, s=1, name="", **kwargs):
        self.b1 = Conv2D(filters= f, kernel_size=k, strides=s, padding='same', name=name+"conv2D")
        self.b2 = BatchNormalization(axis=-1, epsilon=1e-3, name=name+"BN")
        self.b3 = Activation('relu', name=name+ "relu")

    def __call__(self, inputs):
        x = self.b1(inputs)
        x = self.b2(x)
        return self.b3(x)

class Conv:
    """Apply : conv2D """
    def __init__(self, f=1, k=3, s=1, name="", **kwargs):
        self.b1 = Conv2D(filters=f, kernel_size=k, strides=s, padding='same', name=name+"conv2D")

    def __call__(self, inputs):
        return self.b1(inputs)


class DnCNN:
    """ Denoising convolutional neural network architecture """
    def __new__(self, d, f=64, c=1, name="DnCNN",**kwargs):
        self.c = c
        self.name = name
        self.blocks = [ ConvReLU(f, name="CR-") ]
        for i in range(d-2):
            self.blocks.append(ConvBNReLU(f, name="CBR-" + str(i+1) + "-"))
        self.blocks.append( Conv(f=c, name="C-"))
        return self.__call__(self)

    def __call__(self,):
        residual = Input( shape=(None,None, self.c) , name='Input')
        x = residual
        for block in self.blocks:
            x = block(x)
        x = Subtract(name="subtract")([residual,x])
        return Model(inputs=residual, outputs=x, name=self.name)

if __name__=="__main__":
    model = DnCNN(d=6)
    model.summary()
