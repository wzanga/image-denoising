#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 15 16:02:41 2019

@author: zangaw
"""
from tensorflow.keras.layers import Conv2D, Conv2DTranspose, Input, Activation, Add,Subtract, BatchNormalization
import sys
import math
from tensorflow.keras import Model

class ConvReLU:
    """  Sequentially apply : conv2D and ReLU to the inputs """
    def __init__(self, f=64, k=3, s=1, bnorm=True, name="", **kwargs):
        self.b1 = Conv2D(filters= f, kernel_size=k, strides=s, padding='same', kernel_initializer='Orthogonal', use_bias=False, name=name+"conv2D")
        self.b2 = BatchNormalization(momentum=0.0, epsilon=0.001, name=name+"BN")
        self.b3 = Activation('relu', name=name+ "relu")
        self.bnorm = bnorm

    def __call__(self, inputs):
        x = self.b1(inputs)
        if self.bnorm:
            x = self.b2(x)
        return self.b3(x)

class DeconvReLU:
    """  Sequentially apply : deconv2D and ReLU to the inputs """
    def __init__(self, f=64, k=3, s=1, bnorm=False, name="", last=False, **kwargs):
        self.b1 = Conv2DTranspose(filters= f, kernel_size=k, strides=s, padding='same', kernel_initializer='Orthogonal', use_bias=False, name=name+"deconv2D")
        self.b2 = BatchNormalization(momentum=0.0, epsilon=0.001, name=name+"BN")
        self.b3 = Activation('relu', name=name+ "relu")
        self.last = last #last deconv layer ?
        self.bnorm = bnorm

    def __call__(self, inputs):
        x = self.b1(inputs)
        if self.bnorm:
            x = self.b2(x)
        if self.last:
            return x
        return self.b3(x)

class AddReLu:
    """  Sequentially apply : Add ReLU to the inputs """
    def __init__(self,name=""):
        #self.b1 = Subtract(name=name +"sub")
        self.b1 = Add(name=name + 'add')
        self.b2 = Activation('relu', name=name + 'relu')
        return

    def __call__(self,a,b):
        x = self.b1([a,b])
        return self.b2(x)

class REDnet:
    """ REDnet convolutional neural network architecture """
    def __new__(self, d, f=64, c=1, name="REDnet",**kwargs):
        if d %2 !=0:
            sys.exit("The REDNet depth must be an even number !!")
        self.s = d//2
        self.c = c
        #inner-upside-layers
        self.convs = [ ConvReLU(f=f, s=2, name="c" + str(0) +"-") ]
        for i in range(self.s-1):
            self.convs.append( ConvReLU(f=f, name="c" + str(i+1) +"-") )
        #inner-downside-layers
        self.deconvs = []
        for i in range(self.s-1):
            self.deconvs.append( DeconvReLU(f=f,name="d" + str(self.s - i-1) +"-") )
        #final deconv layer
        self.deconvs.append( DeconvReLU(f=c, s=2, last=True, name="d" + str(0) +"-")  )
        return self.__call__(self)

    def __call__(self):
        residual = Input( shape=(None,None, self.c) , name='Input')
        x = residual
        skips =[]
        skip_freq = 2
        # Compute the 1st half-forward
        for i in range(self.s):
            x = self.convs[i](x)
            if (i+1)%skip_freq ==0 and len(skips) < math.ceil(self.s/2)-1:
                skips.append(x)
        # Compute the 2nd half-recursive
        skip_idx = 0
        for i in range(self.s):
            x = self.deconvs[i](x)
            if(i+1+self.s)%skip_freq ==0 and skip_idx < len(skips):
                skip_idx +=1
                x = AddReLu(name="a" + str(self.s -i-1) + "-")( x, skips[ -skip_idx] )
        #add the last skip connection
        x = AddReLu(name="a" + str(0) + "-")(residual,x)
        return Model(inputs=residual, outputs=x, name="REDnet")

if __name__=="__main__":
    model = REDnet(d=10)
    model.summary()
