clean:
	find . -name '*.npy' | xargs rm -f
	find . -name '*.pyc' | xargs rm -f

stage:
	find . -name '*.py' | xargs git add
	find . -name '*.png' | xargs git add
	git add Makefile
